---
layout: job_family_page
title: "Support Engineer"
---

## Support Engineering Roles at GitLab

Support Engineers work with Customers who use GitLab self-managed or in the cloud. They focus on getting bugs and feature proposals triaged and routed to the appropriate team. Support Engineers are often on the vanguard of new and interesting ways GitLab is used, deployed, and managed. Support Engineers sit at the intersection of Systems Administration, DevOps, and Customer Service.

### Support Engineer

Support Engineers are responsible for:

- Resolving customer issues via email and video conferencing
- Collaborating with our Product and Development Teams to build new features and get bugs fixed
- Creating or updating documentation based on customer interactions
- Maintaining good ticket performance and satisfaction
- Rapidly building expert-level knowledge in cutting edge technologies
- Possessing clear technical knowledge and being comfortable explaining technical concepts 
to various audiences
- Participating in the hiring process by reviewing applications and assessments

Support Engineers may focus in one of the following areas:

#### Solutions Support Focus
Support Engineers who focus on Solutions Support primarily work with Customers who use GitLab self-managed. 
They focus on the hard problems of GitLab at scale: performance, 
architecture and finding those weird edge cases that need to get surfaced as 
well-researched bug reports or notes in our documentation.
   
In addition to the above responsibilities, Support Engineers with this focus will:
- Participate in the on-call rotation to provide 24/7 emergency customer response for our self-managed customers

Additional Requirements:
- Excellent Linux systems administration knowledge (LFCE or RHCE equivalent knowledge)
- Excellent Ruby on Rails knowledge; fluency on the Rails console


#### Application Support Focus
Support Engineers who focus on Application Support provide primarily work with GitLab end-users and the company level
administrators who support them. They are experts at the individual features that make up the GitLab application,
and make sure that their expertise translates into customer best-practice. They support a mix of self-managed
and GitLab.com customers.

In addition to the above responsibilities, Support Engineers with this focus will:
- Participate in the GitLab.com Incident Management rotation to provide 24/7 emergency customer response in coordination with the Production team

Additional Requirements:
- Excellent Ruby on Rails knowledge; fluency on the Rails console
- Process oriented: Suggest and implement improvements to the support workflow

### Senior Support Engineer

Senior Support Engineers are more experienced engineers who continue to contribute as an individual,
but operate equally comfortable at the level of team-enablement. That is, they work at the team level
to increase *every member's* ability to contribute. With the wider focus, the specializations of 
the Intermediate role begin to disappear.

Generally they meet the following criteria:

- Involved in mentoring team mates on new technologies and new GitLab features
- Posess expert debugging skills
- Submit merge requests to resolve GitLab bugs
- Drive feature requests based on customer interactions
- Contribute to one or more complementary projects

A Senior Support Engineer may be interested in exploring Support Management as an alternative at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

### Staff Support Engineer

A Senior Support Engineer will be promoted to Staff Support Engineer when they have
demonstrated significant leadership and impact; typically around resolving customer issues. This may
involve any type of consistent "above and beyond senior level" performance, for example:

-  Regularly submitting merge requests for customer reported/requested GitLab bugs and feature proposals
-  Working across functional groups to deliver on projects relating to customer experience and success.
-  Writing in-depth documentation and clarifying community communications that share knowledge and radiate GitLab's technical strengths
-  The ability to create innovative solutions that push GitLab's technical abilities ahead of the curve
-  Identifying significant projects that result in substantial cost savings or revenue
-  Proactively defining and solving important architectural issues based on extensive customer knowledge

## Requirements
### Support Requirements:
- Affinity for (and experience with) providing customer support, and making customers happy
- Thrive in a transactional environment: enjoy working on many small problems in a day.
- Ability to triage and resolve bugs
- Ability to communicate complex technical topics clearly in written and spoken English with customers via tickets, Zoom calls, etc.
- Customer oriented individual; have the ability to adapt and respond to the different personalities and emotional states of our customers
- Experience with support platforms (e.g. Zendesk, SalesForce.com, etc.) preferred
- Experience writing support content
- Experience managing the entire issue lifecycle: from customer, to development team, to resolution

### Technical Requirements:
- Ability to perform complex Linux System Administration tasks
- Professional experience supporting or developing applications in an MVC framework (Ruby on Rails preferred)
- Experience with Git / source control and the software development lifecycle
- Experience with CI/CD

### Other Requirements:
- You share our [values](https://about.gitlab.com/handbook/values), and work in accordance with those values
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
- Spoken and written English and Japanese, Korean or Mandarin for the APAC timezone

## Performance Indicators

Support Engineers have the following job-family Performance Indicators.

* [Customer satisfaction with Support](/handbook/support/performance-indicators/#support-satisfaction-ssat)
* [Maintain at least average monthly tickets](/handbook/support/performance-indicators/#average-daily-tickets-closed-per-support-team-member)
* [Service Level Agreement](/handbook/support/performance-indicators/#service-level-agreement-sla)
* [Ticket deflection through documentation updates](/handbook/support/#ticket-deflection-through-documentation)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates may receive a short questionnaire from our Global Recruiters
* Qualified candidates will be invited to schedule a 30min screening call with our Global Recruiters

* Next, candidates will move to the first round of interviews
  * 90 Minute Technical Interview with a member for the Support team.
    - The Tech Interview will involve live break-fix/bug-fix scenarios as well as customer scenarios.  You will need to have access to a terminal with Bash or similar. You will also need to have an SSH key pair installed locally so you can connect to the server. Windows users must have ‘Git Bash for Windows’ installed prior to the call. If the Tech Interview is not passed, the Behavioral Interview will be canceled.
  * 60 Minute Behavioral Interview
* Next, candidates will move to the second round of interviews
  * 60 Minute Interview with the Director of Support
* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
